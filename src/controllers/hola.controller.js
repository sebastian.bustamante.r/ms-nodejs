"use strict";

const config = require("../config/config");
const holaModule = require("../modules/hola.module");

async function hola(req, res) {
  let response = holaModule();
  return res.status(200).send(response);
}

module.exports = hola;
