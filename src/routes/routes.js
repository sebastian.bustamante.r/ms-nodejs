"use strict";

const { hola } = require("../controllers/hola.controller");

const { healthcheck } = require("../controllers/healthcheck.controller");
const errorHandler = require("../services/errorHandler.service");

function routes(app) {
  app.get("/microservicio/v1/ms-test/hola", hola);

  // Operación de healthcheck
  app.get("/microservicio/v1/ms-test/healthcheck", healthcheck);

  // Middleware para manejo de errores
  app.use(errorHandler);
}

module.exports = { routes };
