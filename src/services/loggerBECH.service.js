const log4js = require("log4js");
const loggerLevel = process.env.LOGGERLEVEL || "info";
const appender = process.env.LOGGERAPPENDER || "stdout";
const nodeEnv = process.env.NODE_ENV;
/* Patrones de formato de logs */
const defaultPattern = "%[[%d %p %f{1}] %X{trackid} %X{codigosesion}%] %m";
const prodPattern = "[%d %p %f{1}] %X{trackid} %X{codigosesion} %m";
const accessPattern = "%[[%d %p]%] %m";

/**
 * Función de configuración global
 */
function loggerConfigure() {
  let logPattern = defaultPattern;

  // En el caso de ambientes productivos
  if (
    nodeEnv === "produccion" ||
    nodeEnv === "production" ||
    nodeEnv === "prod"
  )
    logPattern = prodPattern;

  log4js.configure({
    appenders: {
      out: {
        type: appender,
        layout: {
          type: "pattern",
          pattern: logPattern
        }
      },
      expressOut: {
        type: appender,
        layout: {
          type: "pattern",
          pattern: accessPattern
        }
      }
    },
    categories: {
      default: {
        appenders: ["out"],
        level: loggerLevel,
        enableCallStack: true
      },
      access: { appenders: ["expressOut"], level: loggerLevel }
    }
  });
}

/**
 * Función para obtener un logger con trackId y codigosesion en su contexto
 * @param trackId
 * @param codigosesion
 * @returns {Logger}
 */
function getLoggerBECH(trackId = "", codigosesion = "") {
  const logger = log4js.getLogger("default");
  logger.addContext("trackid", trackId);
  logger.addContext("codigosesion", codigosesion);
  return logger;
}

/**
 * Helper para dar formato al logger express
 * @param req
 * @param res
 * @param format
 */
function formatoExpress(req, res, format) {
  let trackid = req.headers.xtrackid || "";
  let codigosesion = req.headers.codigosesion || "";
  format(
    `:remote-addr - ${trackid} - ${codigosesion} - ":method :url HTTP/:http-version" :status :content-length ":referrer" ":user-agent"`
  );
}

/**
 * Función para obtener un logger express
 * @returns {any}
 */
function getExpress() {
  const logger = log4js.getLogger("access");
  return log4js.connectLogger(logger, {
    level: "auto",
    format: formatoExpress
  });
}

/* Ejecutamos la configuración de log4js cada vez que se instancia esta librería */
loggerConfigure();

const loggerBECHService = getLoggerBECH();
const express = getExpress();

module.exports = { loggerBECH: loggerBECHService, getLoggerBECH, express };
