"use strict";

const test = require("ava");
const rewiremock = require("rewiremock/node");
const MockExpressResponse = require("mock-express-response");
const controllerPath = "../../../src/controllers/hola.controller";

test("hola OK", async t => {
  let mock = rewiremock.proxy(controllerPath, {
    "../../../src/modules/hola.module": () => {
      return {};
    }
  });
  let res = new MockExpressResponse();
  let req = {
    query: {}
  };
  let response = await mock(req, res);
  t.is(response.statusCode, 200);
});
