"use strict";

const test = require("ava");
const rewiremock = require("rewiremock/node");
const servicePath = "../../../src/services/hola.service";

test("hola Service OK", async t => {
  let mock = rewiremock.proxy(servicePath, {});

  let response = await mock();
  t.truthy(response);
});
