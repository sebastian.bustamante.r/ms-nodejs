"use strict";

const test = require("ava");
const rewiremock = require("rewiremock/node");
const modulePath = "../../../src/modules/hola.module";

test("hola Module OK", async t => {
  let mock = rewiremock.proxy(modulePath, {
    "../../../src/services/hola.service": () => {
      return {};
    }
  });
  let response = await mock();
  t.truthy(response);
});
